const express = require('express');
const cors = require('cors');
const parser = require('body-parser');
const morgan = require('morgan');

const app = express();

app.use(cors());
app.use(morgan('combined'));
app.use(parser.json());

app.route('/api/users')
    .get((request, response) => {
        response.status(200)
            .json(request.query);
    });

app.route('/api/users/:userId')
    .get((request, response) => {
        response.status(200)
            .json(request.params);
    });

app.route('/api/test')
    .post((request, response) => {

        const callback = () => {
            const { params, query, body } = request;
            response.status(200)
                .json({ params, query, body });
        };

        const millis = Math.ceil(Math.random() * 9999);
        setTimeout(callback, millis);
    });

module.exports = app;
