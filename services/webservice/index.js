const { createServer } = require('http');
const app = require('./app');

const server = createServer(app);

function listeningCallback() {
    console.log('Server started on port 3000')
}

server.listen(3000, listeningCallback);
